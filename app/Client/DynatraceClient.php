<?php


namespace App\Client;
use GuzzleHttp\Client;

class DynatraceClient
{
    protected $url;
    protected $key;

    public function __construct($url, $key)
    {
        $this->url = $url;
        $this->key = $key;
    }

    /**
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getProblems()
    {
        try {

            $guzzle = new Client();
            $response = $guzzle->request(
                'GET',
                $this->url . 'problem/status',
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Api-token ' . $this->key
                    ]
                ]
            );

            $problemRespose = json_decode($response->getBody()->getContents())->result;
            $totalProblemCount = $problemRespose->totalOpenProblemsCount;

            $sections = [];

            array_push($sections, [
                'type' => 'section',
                'text' => [
                    'type' => 'mrkdwn',
                    'text' => '*Total Issues : ' . $totalProblemCount . '*'
                ]
            ]);


            foreach ($problemRespose->openProblemCounts as $problemType => $problemCount) {

                $problemText = [
                        'type' => 'section',
                        'text' => [
                            'type' => 'mrkdwn',
                            'text' => '*' . $problemType . ':* ' . $problemCount . ' Problems ' . $this->getEmoji($problemCount)
                    ],
                ];

                array_push($sections, $problemText);


                if($problemCount !== 0 ) {
                    $linkAction = [
                        'type' => 'actions',
                        'elements' => [[
                            'type' => 'button',
                            'text' => [
                                'type' => 'plain_text',
                                'text' => 'Get ' . strtolower($problemType) . ' issues',
                                'emoji' => true,
                            ],
                            'value' => $problemType . '||' . $this->url . '||' . $this->key
                        ]]
                    ];
//
                    array_push($sections, $linkAction);
                }
            }

            return $sections;
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param int $problemCount
     * @return string
     */
    public function getEmoji($problemCount)
    {
        return $problemCount ? ':rotating_light:' : ':white_check_mark:';
    }
}


