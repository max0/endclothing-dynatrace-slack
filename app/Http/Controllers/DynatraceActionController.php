<?php

namespace App\Http\Controllers;

use App\Client\DynatraceClient;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
class DynatraceActionController extends Controller
{

    public function __invoke(Request $request)
    {


        $test = $request->get('payload');
        $test = json_decode($test);

        $parts = explode('||', $test->actions[0]->value);
        $type = $parts[0];
        $url = $parts[1];
        $token = $parts[2];

//https://zpt04040.live.dynatrace.com/api/v1/problem/feed?impactLevel=INFRASTRUCTURE&relativeTime=hour
        $guzzle = new Client();
        $response = $guzzle->request(
            'GET',
            $url . 'problem/feed?impactLevel=' . $type . '&relativeTime=hour',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Api-token ' . $token
                ]
            ]
        );

        $problemResponse = json_decode($response->getBody()->getContents())->result;
        $problems = '';
        foreach ( $problemResponse->problems as $problem) {
            //pretty xD
            $problems .=  '<'.str_replace('/api/v1', '', $url)
                .'#problems/problemdetails;pid='
                .$problem->id
                .';gf=all|'
                .$problem->id
                .'> Status : '
                . $problem->status
                . PHP_EOL;
        }

        $slackResponse = [
            'token' => $test->token,
            'response_type' => 'in_channel',
            'channel' => 'DUUHBLADQ',
            'username' => $test->api_app_id,
            'text' => $problems
        ];

        $responseUrl = $test->response_url;

        $ch = curl_init($responseUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($slackResponse));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
//        return JsonResponse::create('ok', JsonResponse::HTTP_OK);
    }
}
