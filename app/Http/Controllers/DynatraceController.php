<?php

namespace App\Http\Controllers;

use App\Client\DynatraceClient;
use Illuminate\Http\JsonResponse;

class DynatraceController extends Controller
{

    public function __invoke()
    {

        $activeEnvironmemts = config('dynatrace.environments');

        $slackResponse = [
            'blocks' =>
                [
                    [
                        'type' => 'section',
                        'text' => [
                            'type' => 'mrkdwn',
                            'text' => 'DYNATRACE Status - checking ' . count($activeEnvironmemts) . ' Applications',
                        ],
                    ],
                    [
                        'type' => 'divider'
                    ],
                ]
        ];

        foreach ($activeEnvironmemts as $dynatraceEnv) {

            array_push($slackResponse['blocks'], [
                'type' => 'section',
                'text' => [
                    'type' => 'mrkdwn',
                    'text' => 'Status for ' . $dynatraceEnv['name'],
                ],
            ]);

            $dynaClient = new DynatraceClient($dynatraceEnv['url'], $dynatraceEnv['key']);
            $serviceStatuses = $dynaClient->getProblems();

            foreach ($serviceStatuses as $serviceStatus) {
                array_push($slackResponse['blocks'], $serviceStatus);
            }

            array_push($slackResponse['blocks'], [
                'type' => 'divider',
            ]);
        }

//        dd($slackResponse);
        return JsonResponse::create($slackResponse, JsonResponse::HTTP_OK);
    }
}
